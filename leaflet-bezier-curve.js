// Requires:
// Leaflet: http://leafletjs.com/
// Leaflet.curve: https://github.com/elfalem/Leaflet.curve
// 
// Assumes:
// var map is a Leaflet map and already set up.

function bezier_draw_curve(map, map_element, lat1, lon1, lat2, lon2, line_color) {

    var latlngs = [];

    var latlng1 = [lat1, lon1],
        latlng2 = [lat2, lon2];

    var offsetX = latlng2[1] - latlng1[1],
        offsetY = latlng2[0] - latlng1[0];

    var r = Math.sqrt( Math.pow(offsetX, 2) + Math.pow(offsetY, 2) ),
        theta = Math.atan2(offsetY, offsetX);

    var thetaOffset = (3.14/10);

    var r2 = (r/2)/(Math.cos(thetaOffset)),
        theta2 = theta + thetaOffset;

    var midpointX = (r2 * Math.cos(theta2)) + latlng1[1],
        midpointY = (r2 * Math.sin(theta2)) + latlng1[0];

    var midpointLatLng = [midpointY, midpointX];

    latlngs.push(latlng1, midpointLatLng, latlng2);

    var pathOptions = {
        color: line_color,
        weight: 2
    }

    if (typeof map_element.animate === "function") { 
        pathOptions.animate = {
            duration: 500,
            easing: 'ease-in-out',
        }
    }

    var curvedPath = L.curve(
        [
            'M', latlng1,
            'Q', midpointLatLng,
                 latlng2
        ], pathOptions).addTo(map);

    return curvedPath;
}