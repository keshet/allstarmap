#!/usr/local/bin/python3
import cgi
import json
import sys
import urllib.request

arguments = cgi.FieldStorage()
url = "http://stats.allstarlink.org/api/stats/" + str(arguments['n'].value)
f = urllib.request.urlopen(url)
encoding = f.info().get_content_charset('utf-8')
contents = f.read()
decoded = contents.decode(encoding)

all_data = json.loads(decoded)

node_list = []
try:
    nodes = all_data['stats']['data']['linkedNodes']
    for node in nodes:
        if 'Node_ID' in node and 'name' in node:
            node_list.append(node['name'])
except Exception as ex:
    pass

print("Content-Type: text/plain")
print('')
for node in node_list:
    print(node)
