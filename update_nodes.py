import datetime
import json
import os
import sys

ALL_NODES_JSON_FILE = 'all_nodes.json'
FIELDS = ['id', 'call', 'lat', 'lon', 'freq', 'tone', 'name']
TMP_JS_FILE = 'tmp_node_file.js'

#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////

def binary_to_string(bstring):
    result = ''.join(chr(x) for x in bstring)
    return result

#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////

def parse_str(token):
    result = token
    try:
        if isinstance(token, bytes):
            result = binary_to_string(token)
    except:
        result = str('')
    return result

#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////

def parse_id(token):
    try:
        result = int(token)
    except:
        result = 0
    return result

#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////

def parse_float(token):
    try:
        result = float(token)
    except:
        result = parse_str(token)
    return result

#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////

def load_all_nodes(all_nodes_file):

    with open(all_nodes_file, 'r') as f:
        all_nodes = json.loads(f.read())
    for node in all_nodes:
        for k in node.keys():
            if isinstance(node[k], bytes):
                node[k] = binary_to_string(node[k])
            if isinstance(node[k], str):
                node[k] = node[k].replace('\\', '/')
                node[k] = '' if node[k] == '<None>' else node[k]
    return all_nodes

#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////

def parse_online_node(line):
    tokens = [t.strip() for t in line.split('\t')]
    tokens = list(filter(lambda t: t != '', tokens))
    while len(tokens) < 7:
        tokens.append('')
    node = {
        'id': parse_id(tokens[0]),
        'call': parse_str(tokens[1]),
        'lat': parse_float(tokens[2]),
        'lon': parse_float(tokens[3]),
        'freq': parse_str(tokens[5]),
        'tone': parse_str(tokens[6]),
        'name': parse_str(tokens[4]),
    }
    for k in node.keys():
        node[k] = '' if node[k] == '<None>' else node[k]
        if isinstance(node[k], str):
            node[k] = node[k].replace('\\', '/')
    return node

#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////

def load_online_nodes(online_nodes_file):
    lines = [line.strip() for line in open(online_nodes_file, 'r')] 
    nodes = [parse_online_node(line) for line in lines]
    nodes = list(filter(lambda n: n['id'] > 0, nodes))
    return nodes

#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////

def save_nodes_to_js(nodes, js_var_name, out_file):
    with open(TMP_JS_FILE, 'w') as f:
        f.write('var {} = [\n'.format(js_var_name))
        for node in nodes:
            for k in node.keys():
                node[k] = '' if node[k] == '<None>' else node[k]
                if isinstance(node[k], str):
                    node[k] = node[k].replace('\\', '/')
            f.write('{{ id: {}, call: "{}", lat: {}, lon: {}, freq: "{}", tone: "{}", name: "{}", isup: {} }},\n'.format(
                node['id'],
                node['call'],
                node['lat'],
                node['lon'],
                node['freq'],
                node['tone'],
                node['name'],
                'true' if node['isup'] else 'false',
            ))
        f.write('];\n')

    if os.path.isfile(out_file):
        os.remove(out_file)
    os.rename(TMP_JS_FILE, out_file)

#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////

def save_all_nodes_to_js(all_nodes, online_nodes, out_file):
    online_ids = [node['id'] for node in online_nodes]

    for node in all_nodes:
        node['isup'] = True if node['id'] in online_ids else False

    save_nodes_to_js(all_nodes, 'g_all_nodes', 'all_nodes.js')

#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////

def save_online_nodes_to_js(online_nodes, out_file):
    for node in online_nodes:
        node['isup'] = True

    save_nodes_to_js(online_nodes, 'g_online_nodes', 'all_online_nodes.js')

#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////

def save_all_nodes_to_json(all_nodes, out_file):
    with open(out_file, 'w') as f:
        f.write('[\n')
        total = len(all_nodes)
        for node in all_nodes:
            f.write('{ ')
            for k in node.keys():
                if isinstance(node[k], str):
                    node[k] = node[k].replace('\\', '/')
            f.write('"id": {}, "call": "{}", "lat": {}, "lon": {}, "freq": "{}", "tone": "{}", "name": "{}"'.format(
                node['id'], node['call'], node['lat'], node['lon'], node['freq'], node['tone'], node['name']
            ))
            total -= 1
            if total > 0:
                f.write(' },\n')
            else:
                f.write(' }\n')
        f.write(']\n')

#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////

def update_node_for_float_property(node, online_node, key):
    if isinstance(online_node[key], float):
        node[key] = online_node[key]
    else:
        node[key] = online_node[key] if node[key] == '' else node[key]

def update_node(node, online_node):
    ### 0,0 mean no lat,lon
    try:
        if float(node['lat']) == 0 and float(node['lon']) == 0:
            node['lat'] = ''
            node['lon'] = ''
    except Exception as ex:
        node['lat'] = ''
        node['lon'] = ''

    ### Only update numbers if they are valid OR existing property is empty
    update_node_for_float_property(node, online_node, 'lat')
    update_node_for_float_property(node, online_node, 'lon')
    update_node_for_float_property(node, online_node, 'freq')
    update_node_for_float_property(node, online_node, 'tone')

    ### Update name if was empty
    node['name'] = online_node['name'] if node['name'] == '' else node['name']

#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////

def print_node(node):
    for f in FIELDS:
        if isinstance(node[f], str):
            sys.stdout.write('    {}: {}, '.format(f, node[f].encode('utf-8')))
        else:
            sys.stdout.write('    {}: {}, '.format(f, node[f]))
    sys.stdout.write('\n')

def print_diff(n1, n2):
    print('---------------------------------------------')
    print_node(n1)
    print('   \\/')
    print_node(n2)

def show_diff(n1, n2):
    for f in FIELDS:
        if n1[f] != n2[f]:
            print_diff(n1, n2)
            continue

#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////

def update_all_nodes(all_nodes, online_nodes):
    ### make a dictionary of all nodes
    all_nodes_dict = {}
    for node in all_nodes:
        all_nodes_dict[node['id']] = node

    ### update existing nodes from online, add missing
    for index, node in enumerate(online_nodes):
        nid = node['id']
        if nid in all_nodes_dict:
            # DEBUG old_copy = all_nodes_dict[nid].copy()
            ### Update missing fields
            update_node(all_nodes_dict[nid], node)
            ### Set online node fields to all nodes info
            online_nodes[index] = all_nodes_dict[nid]
            # DEBUG show_diff(old_copy, all_nodes_dict[nid])
        else:
            all_nodes.append(node)
            # print('---------------------------------------------')
            # print('add missing node:')
            # print_node(node)

#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////

if __name__ == "__main__":
    all_nodes = load_all_nodes(ALL_NODES_JSON_FILE)
    online_nodes = load_online_nodes('mapdata.txt')

    if len(online_nodes) > 200:
        ### Update all nodes with data from currently online nodes and save
        update_all_nodes(all_nodes, online_nodes)
        save_all_nodes_to_json(all_nodes, ALL_NODES_JSON_FILE)

        ### Update js status files
        save_online_nodes_to_js(online_nodes, 'all_online_nodes.js')
        save_all_nodes_to_js(all_nodes, online_nodes, 'all_nodes.js')
        print('{} {}'.format(str(datetime.datetime.now())[:19], len(online_nodes)))
    else:
        print('{} {} (FAIL)'.format(str(datetime.datetime.now())[:19], len(online_nodes)))
        sys.exit(1)
