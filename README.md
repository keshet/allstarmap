Allstar Map
===========

This is the source code for the AllStarLink node map at http://allstarmap.org


Node Information
----------------
`update_nodes.sh` is run once an hour. It:

1. will download the list of currently connected allstar nodes from http://stats.allstarlink.org/api/stats/mapData to `mapdata.txt`

2. run the script `update_nodes.py` which reads from `mapdata.txt`

3. `update_nodes.py` will add any missing nodes to `all_nodes.json` and save the online nodes in `all_online_nodes.js`


Web Application
---------------
`index.html`:

1. loads `all_online_nodes.js` giving it the node list in the `g_online_nodes` variable

2. Uses the leaflet API to load maps and a layer showing the nodes.

3. when a user clicks "get connections" calls `fetch_node_status.py` asynchronously to get the connections for a specific node which uses the `http://stats.allstarlink.org/api/stats/` endpoint

4. Loads all the nodes asynchronously from `all_nodes.js` when Show All Nodes is selected in the settings sidebar tab
