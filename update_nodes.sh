#!/bin/sh
online_list_time=`date -u +"%H:%M UTC %d-%b-%y"`
curl -s http://stats.allstarlink.org/api/stats/mapData > mapdata.txt

python3 update_nodes.py >> update.log
if [ $? -ne 0 ] ; then
	echo "Node update failed"
    tail -n 1 update.log
    exit 1
fi

echo "var online_status_timestamp = \"$online_list_time\";" > online_status_time.js
echo "Nodes updated OK"
tail -n 1 update.log
exit 0
